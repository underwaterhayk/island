package com.kirakossian.island.scripts;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.kirakossian.island.Facade;


public class ScoreScript extends Group {
    private Label scoreLabel;
    private int score;

    public ScoreScript() {
        scoreLabel  =   new Label("0", Facade.getGame().getLabelStyle(30));
        setWidth(200);
        setHeight(50);
        addActor(scoreLabel);
        scoreLabel.setX(-scoreLabel.getWidth()/2);
    }
    public void setScore(){
        score+=1;
        scoreLabel.setText(score+"");
    }
}
