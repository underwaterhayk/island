package com.kirakossian.island.scripts;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.kirakossian.island.Facade;
import com.kirakossian.island.logic.beuaivours.*;


public class SpellPanelScript extends Group {

    private Array<SlotScript> spellButtons   =   new Array<SlotScript>();
    private Array<Spell>    allSpells   =   new Array<Spell>();
    private Table table =   new Table();
    public SpellPanelScript() {
        setWidth(Facade.getGame().getCurrentScreen().getStage().getWidth());
        setHeight(100);

        initSpells();
        initButtons();

        addActor(table);
        table.setFillParent(true);
    }

    private void initSpells() {
        allSpells.add(new FireSpell());
        allSpells.add(new DoubleFireSpell());
        allSpells.add(new GroundDestroySpell());
        allSpells.add(new AirDestroySpell());
    }

    private void initButtons() {
        for (int i = 0; i < 4; i++) {
            SlotScript spellBtn   =   new SlotScript(allSpells.get(i));
            spellButtons.add(spellBtn);
            table.add(spellBtn).fill().expand();


        }
    }


}
