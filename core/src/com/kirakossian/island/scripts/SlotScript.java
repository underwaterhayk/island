package com.kirakossian.island.scripts;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.kirakossian.island.Facade;
import com.kirakossian.island.logic.beuaivours.FireSpell;
import com.kirakossian.island.logic.beuaivours.Spell;


public class SlotScript extends Group{

    private static final float WIDTH = 90;
    private static final float HEIGHT = 90;
    private CooldownDrawable icon;

    private Vector2 mainUV = new Vector2();
    private Vector2 mainU2V2 = new Vector2();

    private Vector2 gradientUV = new Vector2();
    private Vector2 gradientU2V2 = new Vector2();
    private Spell currentSpell;
    private boolean isSpellinCooldown   =   false;
    private float cooldown;



    public SlotScript(Spell spell) {
        TextureRegion gradientTexture = Facade.getGame().getRm().getTextureRegion("ui-spell-cooldown");
        gradientUV.set(gradientTexture.getU(), gradientTexture.getV2());
        gradientU2V2.set(gradientTexture.getU2(), gradientTexture.getV());

        setWidth(WIDTH);
        setHeight(HEIGHT);

        this.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                if(!isSpellinCooldown) {
                    addspell();
                }
            }
        });

        currentSpell    =   spell;
        build();
    }

    private void addspell() {
        isSpellinCooldown   =   true;
        Facade.getGame().getBehaivourManager().addBehaivour(currentSpell);
    }
    private void build() {
        icon = new CooldownDrawable(Facade.getGame().getRm().getTextureRegion(currentSpell.getRegion()));
        addActor(icon);
    }

    @Override
    public void act(float delta) {
        if (!Facade.getGame().paused && isSpellinCooldown) {
            cooldown+=delta;
            if(cooldown >=   currentSpell.getDurration()){
                isSpellinCooldown   =   false;
                cooldown=0;
            }
        }
    }

    public class CooldownDrawable extends Actor{

        private TextureRegion region;

        private float U, V, U2, V2;

        private Vector2 working = new Vector2();

        private float[] verts = new float[5 * 3];
        private short[] shorts = new short[] {0, 1, 2};

        private Color temp = new Color(1f, 1f, 1f, 1f);
        private float whiteBits = temp.toFloatBits();

        private float progress;


        public CooldownDrawable(TextureRegion region) {
            this.region = region;

            U = region.getU();
            V = region.getV();
            U2 = region.getU2();
            V2 = region.getV2();
        }


        @Override
        public void draw (Batch batch, float parentAlpha) {

            progress = 1;
            if (!Facade.getGame().paused && isSpellinCooldown) {
                progress = cooldown / (currentSpell.getDurration()*1000f);
                if (progress < 0) progress = 0;
            }
            render((PolygonSpriteBatch)batch, region, getX(), getY(),WIDTH, HEIGHT, progress*1000);
        }


        private void render (PolygonSpriteBatch polygonSpriteBatch, TextureRegion back, float x, float y, float width, float height, float percentage) {
            polygonSpriteBatch.setColor(0.2f, 0.2f, 0.2f, 1f);
            polygonSpriteBatch.draw(back, x, y, width, height);
            polygonSpriteBatch.setColor(1f, 1f, 1f, 1f);


            //Set the darkness of the icon when in progress
            if (progress < 1f) {
                whiteBits = temp.set(1f, 1f, 1f, 1f).toFloatBits();
            } else {
                whiteBits = temp.set(1f, 1f, 1f, 1f).toFloatBits();
            }

            int fullSegments = (int)(percentage / (1 / 8f));

            float percentageLastSegment = (percentage - (1 / 8f * fullSegments)) / (1 / 8f);

            for (int i = 0; i < fullSegments; i++) {
                float angle = i * 45f;
                float nextAngle = (i + 1) * 45f;

                working.set(0, height * 2f);
                working.rotate(-angle);


                getIntersectionLineRectangle(working.x, working.y, -width/2f, -height/2f, width, height, working);

                snap(working, width, height);

                float startX = working.x;
                float startY = working.y;


                working.set(0, height * 2f);
                working.rotate(-nextAngle);

                getIntersectionLineRectangle(working.x, working.y, -width/2f, -height/2f, width, height, working);

                snap(working, width, height);

                float endX = working.x;
                float endY = working.y;

                setToTriangle(x, y, width, height, startX, startY, endX, endY);
                polygonSpriteBatch.draw(back.getTexture(), verts, 0, verts.length, shorts, 0, 3);
            }

            if (fullSegments < 8) {

                float current = (fullSegments) * 45f;

                working.set(0, height * 2f);
                working.rotate(-current);

                getIntersectionLineRectangle(working.x, working.y, -width/2f, -height/2f, width, height, working);
                snap(working, width, height);

                float currentX = working.x;
                float currentY = working.y;

                working.set(0, height * 2f);
                working.rotate(-(current + (percentageLastSegment * 45f)));

                getIntersectionLineRectangle(working.x, working.y, -width/2f, -height/2f, width, height, working);
                snap(working, width, height);

                float finalX = working.x;
                float finalY = working.y;


                setToTriangle(x, y, width, height, currentX, currentY, finalX, finalY);
                polygonSpriteBatch.draw(back.getTexture(), verts, 0, verts.length, shorts, 0, 3);
            }

        }

        private void snap (Vector2 vector2, float targetWidth, float targetHeight) {
            if (MathUtils.isEqual(0, vector2.x, 1f)) vector2.x = 0;
            if (MathUtils.isEqual(0, vector2.y, 1f)) vector2.y = 0;

            if (MathUtils.isEqual(targetWidth, vector2.x, 1f)) vector2.x = targetWidth;
            if (MathUtils.isEqual(targetWidth, vector2.y, 1f)) vector2.y = targetWidth;

            if (MathUtils.isEqual(targetHeight, vector2.x, 1f)) vector2.x = targetHeight;
            if (MathUtils.isEqual(targetHeight, vector2.y, 1f)) vector2.y = targetHeight;
        }

        private float snap (float value, float target, float target2) {
            if (MathUtils.isEqual(value, target, 0.001f)) return target;
            if (MathUtils.isEqual(value, target2, 0.001f)) return target2;

            return value;
        }

        private void getIntersectionLineRectangle (float endX, float endY, float rectX, float rectY, float rectWidth, float rectHeight, Vector2 working) {
            if (Intersector.intersectSegments(0, 0, endX, endY, rectX, rectY, rectX + rectWidth, rectY, working)) return;
            if (Intersector.intersectSegments(0, 0, endX, endY, rectX, rectY + rectHeight, rectX + rectWidth, rectY + rectHeight, working)) return;

            if (Intersector.intersectSegments(0, 0, endX, endY, rectX, rectY, rectX, rectY + rectHeight, working)) return;
            if (Intersector.intersectSegments(0, 0, endX, endY, rectX + rectWidth, rectY, rectX + rectWidth, rectY + rectHeight, working)) return;

        }

        private void setToTriangle (float x, float y, float width, float height, float startX, float startY, float endX, float endY) {
            verts[0] = x + width/2f;
            verts[1] = y + height/2f;
            verts[2] = whiteBits;
            verts[3] = mapU(0.5f);
            verts[4] = mapV(0.5f);

            verts[5] = startX + x + width/2f;
            verts[6] = startY + y + height/2f;
            verts[7] = whiteBits;
            verts[8] = mapU((startX + width / 2f) / width);
            verts[9] = mapV((1f - startY + height / 2f) / height);

            verts[10] = endX + x + width/2f;
            verts[11] = endY + y + height/2f;
            verts[12] = whiteBits;
            verts[13] = mapU((endX + width / 2f) / width);
            verts[14] = mapV(1f - (endY + height / 2f) / height);
        }

        private float mapU (float inU) {

            float u = U + ((U2 - U) * inU);
            return snap(u, U, U2);
        }

        private float mapV (float inV) {
            float v = V + ((V2 - V) * inV);
            return snap(v, V, V2);
        }
    }
}
