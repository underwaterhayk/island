package com.kirakossian.island.scripts;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;
import com.kirakossian.island.Facade;
import com.kirakossian.island.Island;
import com.kirakossian.island.logic.actors.ParticleActor;
import com.kirakossian.island.stages.GameStage;


/**
 * Created by kirakos on 2/13/18.
 */
public class BonusProgressBarScript extends Group {
    public static final int MAX_PROGRESS = 10;
    private int progress;
    private Array<Image> itemImagesList =   new Array<Image>();
    private Image bodyImage;
    private Image bodyImageBlank;
    private ParticleActor bodyparticleActor;
    private ParticleActor completeParticeleActor;
    private Group itemsGroup    =   new Group();

    public BonusProgressBarScript(){
        completeParticeleActor  =   new ParticleActor("coin-boost-complete-pe",1);
        bodyparticleActor       =   new ParticleActor("boost-item-particle",1);
        bodyparticleActor.start();
        bodyImage       =   new Image(Facade.getGame().getRm().getTextureRegion("progress-bar-body"));
        bodyImageBlank  =   new Image(Facade.getGame().getRm().getTextureRegion("progress-bar-body-blank"));

        bodyparticleActor.setPosition(bodyImage.getWidth()/2, 20);

        completeParticeleActor.setPosition(bodyImage.getWidth()/2+3, 20);
        addActor(itemsGroup);
        itemsGroup.addActor(bodyImageBlank);
        bodyImageBlank.getColor().a =   0f;
        addActor(bodyparticleActor);

        addActor(bodyImage);
        setWidth(bodyImage.getWidth());
        setHeight(bodyImage.getHeight());
    }
    public void addProgress(){
        progress++;
        Image progressItemIcon    =   new Image(Facade.getGame().getRm().getAtlasRegion("progress-bar-item"));
        addActor(progressItemIcon);
        progressItemIcon.setX((int)(getWidth()/2-progressItemIcon.getWidth()/2));
        progressItemIcon.setY(35 +  itemImagesList.size*progressItemIcon.getHeight());
        itemImagesList.add(progressItemIcon);

        bodyImageBlank.clearActions();
        bodyImageBlank.addAction(Actions.sequence(
                Actions.fadeIn(0.1f, Interpolation.pow2),
                Actions.fadeOut(0.2f, Interpolation.pow2)
        ));

        if(progress==MAX_PROGRESS){
            reset();
            completeParticeleActor.start();
            itemsGroup.addActor(completeParticeleActor);
            ((GameStage)(Facade.getGame().getCurrentScreen().getStage())).getHero().setShield();
        }

    }
    public void reset() {
        for (int i = 0; i < itemImagesList.size; i++) {
            itemImagesList.get(i).remove();
        }
        progress    =   0;
        itemImagesList.clear();
    }
}
