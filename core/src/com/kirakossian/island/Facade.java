package com.kirakossian.island;

import com.badlogic.gdx.utils.Array;

import java.util.MissingResourceException;


public class Facade {

    private static Facade instance = null;


    private Island game;

    protected Facade() {

    }

    private static Facade get() {
        if(instance == null) {
            instance = new Facade();
        }
        return instance;
    }

    public static void setGame(Island game) {
        get().game = game;
    }
    public static Island getGame() {
        return get().game;
    }


    public static void dispose() {
        if(instance==null){
            return;
        }
        instance.game = null;
        instance  = null;
    }
}
