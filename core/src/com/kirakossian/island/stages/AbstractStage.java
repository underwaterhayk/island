package com.kirakossian.island.stages;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.ScalingViewport;

public class AbstractStage extends Stage {


    public AbstractStage() {
        super(new ScalingViewport(Scaling.stretch, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), new OrthographicCamera()),
                new PolygonSpriteBatch(3000));
        initViewPort();
    }

    private void initViewPort() {
        float vpWidth = 480;
        float vpHeight = vpWidth * Gdx.graphics.getHeight() / Gdx.graphics.getWidth();
        FitViewport viewport = new FitViewport(vpWidth, vpHeight);
        setViewport(viewport);
        viewport.update(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), true);
    }

    @Override
    public void draw() {
        super.draw();
        getViewport().apply();
    }

    @Override
    public void dispose() {
        super.dispose();
    }
}
