package com.kirakossian.island.stages;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.kirakossian.island.Facade;
import com.kirakossian.island.logic.Hero;
import com.kirakossian.island.logic.actors.Land;
import com.kirakossian.island.logic.actors.ParticleActor;
import com.kirakossian.island.logic.beuaivours.BehaivourManager;
import com.kirakossian.island.logic.beuaivours.MoveBehaivour;
import com.kirakossian.island.logic.data.DirectionState;

public class GameStage extends AbstractStage {
    private  Land land;
    private Hero hero;
    private Actor animActor;
    private UIStage uiStage;

    public GameStage() {
        super();

        animActor   =   new Actor();
        addActor(animActor);

        ParticleActor smokeParticleActor = new ParticleActor("smoke",1);
        smokeParticleActor.setPosition(getWidth()/2, 0);
        smokeParticleActor.start();
        addActor(smokeParticleActor);

        land    =   new Land();
        land.setPosition(getWidth()/2-land.getWidth()/2, 0);
        addActor(land);

        hero    =   new Hero(this);
        land.addActor(hero);


//        ParticleActor particleActor = new ParticleActor("cloud-circle");
//        particleActor.setPosition(getWidth()/2, 670);
//        particleActor.start();
//        addActor(particleActor);

        initMovement();

        addListener(new InputListener(){
            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                if(keycode == Input.Keys.LEFT){
                    uiStage.getBonusProgressBarScript().addProgress();
                    hero.setShield();
                }
                return super.keyDown(event, keycode);
            }
        });

    }

    private void initMovement() {
        this.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                if(x>getWidth()/2){
                    if(y>getHeight()/2){
                        MoveBehaivour moveBehaivour =   new MoveBehaivour(DirectionState.UP_RIGHT, hero);
                        Facade.getGame().getBehaivourManager().addBehaivour(moveBehaivour);
                        moveBehaivour.end();
                    }else{
                        MoveBehaivour moveBehaivour =   new MoveBehaivour(DirectionState.DOWN_RIGHT, hero);
                        Facade.getGame().getBehaivourManager().addBehaivour(moveBehaivour);
                        moveBehaivour.end();
                    }
                }else{
                    if(y>getHeight()/2){
                        MoveBehaivour moveBehaivour =   new MoveBehaivour(DirectionState.UP_LEFT, hero);
                        Facade.getGame().getBehaivourManager().addBehaivour(moveBehaivour);
                        moveBehaivour.end();
                    }else{
                        MoveBehaivour moveBehaivour =   new MoveBehaivour(DirectionState.DOWN_LEFT, hero);
                        Facade.getGame().getBehaivourManager().addBehaivour(moveBehaivour);
                        moveBehaivour.end();
                    }
                }
            }
        });
    }

    @Override
    public void act() {
        super.act();
    }

    @Override
    public void draw() {
        super.draw();
    }

    public Land getLand() {
        return land;
    }

    public Hero getHero() {
        return hero;
    }

    public Actor getActionActor() {
        return animActor;
    }

    public void setUIStage(UIStage uiStage) {
        this.uiStage    =   uiStage;
    }

    public UIStage getUiStage() {
        return uiStage;
    }
}
