package com.kirakossian.island.stages;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.kirakossian.island.Facade;
import com.kirakossian.island.logic.actors.SpineActor;
import com.kirakossian.island.scripts.SpellPanelScript;

public class GameOverStage extends AbstractStage {
    private Label gameOverLabel;
    private SpineActor heroSpine;
    private SpineActor enemySpine;

    public GameOverStage() {
        super();
        gameOverLabel   =   new Label("GAME OVER", Facade.getGame().getLabelStyle(30));
        gameOverLabel.setX(getWidth()/2-gameOverLabel.getWidth()/2);
        gameOverLabel.setY(20);
        addActor(gameOverLabel);


        addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
               super.clicked(event, x, y);
               Facade.getGame().setGameScreen();
            }
        });

        //init spines
        heroSpine   = new SpineActor("asteroid-mining-bot");
        addActor(heroSpine);
        heroSpine.setX(getWidth()/2);
        heroSpine.setY(getHeight()/2-getHeight()/4);
    }



    @Override
    public void act() {
        super.act();
    }

    @Override
    public void draw() {
        super.draw();
    }
}
