package com.kirakossian.island.stages;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.kirakossian.island.logic.Hero;
import com.kirakossian.island.logic.actors.Land;
import com.kirakossian.island.logic.actors.ParticleActor;
import com.kirakossian.island.scripts.BonusProgressBarScript;
import com.kirakossian.island.scripts.ScoreScript;
import com.kirakossian.island.scripts.SpellPanelScript;

public class UIStage extends AbstractStage {
    private  BonusProgressBarScript bonusProgressBarScript;
    private SpellPanelScript spellPanelScript;
    private ScoreScript scoreScript;

    public UIStage() {
        super();
        bonusProgressBarScript  =   new BonusProgressBarScript();
        bonusProgressBarScript.setX(10);
        bonusProgressBarScript.setY(getHeight()-bonusProgressBarScript.getHeight()-10);
        addActor(bonusProgressBarScript);

        scoreScript =   new ScoreScript();
        scoreScript.setX(getWidth()/2);
        scoreScript.setY(getHeight()-scoreScript.getHeight()-10);
        addActor(scoreScript);
    }

    public void initSpellPanel() {
        spellPanelScript    =new SpellPanelScript();
        addActor(spellPanelScript);



    }

    @Override
    public void act() {
        super.act();
    }

    @Override
    public void draw() {
        super.draw();
    }

    public BonusProgressBarScript getBonusProgressBarScript() {
        return bonusProgressBarScript;
    }

    public ScoreScript getScoreScript() {
        return scoreScript;
    }
}
