package com.kirakossian.island.stages;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.kirakossian.island.logic.Hero;
import com.kirakossian.island.logic.actors.Land;

public class SplashStage extends AbstractStage {
    private  Land land;
    private Hero hero;
    public SplashStage() {
       super();

    }

    @Override
    public void act() {
        super.act();
    }

    @Override
    public void draw() {
        super.draw();
    }
}
