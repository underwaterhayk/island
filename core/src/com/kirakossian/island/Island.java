package com.kirakossian.island;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.kirakossian.island.logic.beuaivours.BehaivourManager;
import com.kirakossian.island.resources.GameResourceManager;
import com.kirakossian.island.screens.AbstractScreen;
import com.kirakossian.island.screens.GameOverScreen;
import com.kirakossian.island.screens.GameScreen;
import com.kirakossian.island.screens.SplashScreen;
import com.kirakossian.island.stages.GameStage;


public class Island extends Game {

	public boolean paused;
	private GameResourceManager gameResourceManager;

	private SplashScreen splashScreen;

	private AbstractScreen activeScreen;

	private BehaivourManager behaivourManager;
	private GameScreen gamescreen;

	private Label.LabelStyle labelStyle_14;
	private Label.LabelStyle labelStyle_24;
	private Label.LabelStyle labelStyle_30;

	@Override
	public void create () {
		gameResourceManager	=	new GameResourceManager();
		gameResourceManager.load();



		splashScreen	=	new SplashScreen(this);
		setScreen(splashScreen);
		activeScreen	=	splashScreen;

	}
	private void initLabelStyle() {
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/Roboto-Medium.ttf"));
		FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
		parameter.size = 14;
		BitmapFont font_14 = generator.generateFont(parameter);
		labelStyle_14	=	new Label.LabelStyle(font_14, Color.WHITE);
		parameter.size = 24;
		BitmapFont font_24 = generator.generateFont(parameter);
		labelStyle_24	=	new Label.LabelStyle(font_24, Color.WHITE);
		parameter.size = 30;
		BitmapFont font_30 = generator.generateFont(parameter);
		labelStyle_30	=	new Label.LabelStyle(font_30, Color.WHITE);

		generator.dispose(); // don't forget to dispose to avoid memory leaks!
	}
	public Label.LabelStyle getLabelStyle(int size) {
		if(size==14){
			return labelStyle_14;
		}else if(size==24){
			return labelStyle_24;
		}else if(size==30){
			return labelStyle_30;
		}else{
			return labelStyle_24;
		}
	}
	@Override
	public void render () {
		super.render();


	}
	
	@Override
	public void dispose () {
	}

	public boolean isPaused() {
		return false;
	}

	public GameResourceManager getRm() {
		return gameResourceManager;
	}

	public void prepareGame() {
        initLabelStyle();
		Facade.setGame(this);
		behaivourManager    =   new BehaivourManager();
	}

	public void setGameScreen() {
		gamescreen 		= new GameScreen();
		activeScreen	=	gamescreen;
		setScreen(gamescreen);
		gamescreen.getUiStage().initSpellPanel();
	}
	public AbstractScreen getCurrentScreen(){
		return activeScreen;
	}

	public BehaivourManager getBehaivourManager() {
		return behaivourManager;
	}

	public void gameOver() {
		behaivourManager.reset();
		setScreen(new GameOverScreen());
	}
}
