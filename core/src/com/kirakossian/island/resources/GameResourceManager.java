package com.kirakossian.island.resources;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.ParticleEffectLoader;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.ObjectMap;
import com.esotericsoftware.spine.AnimationStateData;
import com.esotericsoftware.spine.SkeletonData;
import com.esotericsoftware.spine.SkeletonJson;
import com.esotericsoftware.spine.SkeletonMeshRenderer;
import com.kirakossian.island.resources.loaders.MappedTextureAtlasLoader;
import com.kirakossian.island.resources.loaders.SpineAnimationLoader;

/**
 * Created by kirakos on 12/17/17.
 */
public class GameResourceManager {


    private AssetManager assetManager;

    private TextureAtlas textureAtlas;
    private ObjectMap<String, ParticleEffect> particles = new ObjectMap<String, ParticleEffect>();
    private ObjectMap<String, Music> musics = new ObjectMap<String, Music>();
    private ObjectMap<String, Sound> sounds = new ObjectMap<String, Sound>();
    private ObjectMap<String, SpineAnimation> spineAnimations = new ObjectMap<String, SpineAnimation>();



    private final String particlesPath  = "particles/";
    private final String musicPath      = "music/";
    private final String soundsPath     = "sounds/";
    private final String spinesPath     = "spines/";
    private Array<String> particlesList =   new Array<String>();
    private Array<String> musicList     =   new Array<String>();
    private Array<String> soundsList    =   new Array<String>();
    private Array<String> spinesList    =   new Array<String>();
    private String audioExtension   =   ".mp3";
    private SkeletonMeshRenderer skeletonMeshrenderer;

    public GameResourceManager( ) {
       assetManager =   new AssetManager();

        assetManager.setLoader(SpineAnimation.class, new com.kirakossian.island.resources.loaders.SpineAnimationLoader(assetManager.getFileHandleResolver()));
        assetManager.setLoader(MappedTextureAtlas.class, new MappedTextureAtlasLoader(assetManager.getFileHandleResolver()));

       initParticles();
//       initMusic();
//       initSounds();
       initSpines();
        skeletonMeshrenderer    =   new SkeletonMeshRenderer();
    }

    private void initSpines() {
       spinesList.add("alien-boss");
       spinesList.add("asteroid-mining-bot");
       spinesList.add("bot-scientist");
       spinesList.add("hero-right-front");
       spinesList.add("zombie-right-front");
    }

    private void initSounds() {
        soundsList.add("click");
        soundsList.add("boost");
        soundsList.add("take");
        soundsList.add("angel");
        soundsList.add("coins");
        soundsList.add("coin-take");
        soundsList.add("bonus-take");
        soundsList.add("lose");
        soundsList.add("timer-slow");
        soundsList.add("start");
    }

    private void initMusic() {
        musicList.add("menu");
        musicList.add("game1");
        musicList.add("game2");
        musicList.add("game3");
        musicList.add("game4");
    }

    private void initParticles() {
        // TODO: 12/17/17  here add particles to maps

        particlesList.add("cloud-circle");
        particlesList.add("portal-circle");
        particlesList.add("smoke");
        particlesList.add("boost-item-particle");
        particlesList.add("coin-boost-complete-pe");
        particlesList.add("shield");
        particlesList.add("shieldRemove");
        particlesList.add("fireCollision");
        particlesList.add("ground-explossion");
        particlesList.add("earthquake-effect");
        particlesList.add("air-attack-particle");
        particlesList.add("fire-smoke");
        particlesList.add("fire-smoke-left");
        particlesList.add("groud-attack-pe");
        particlesList.add("zombie-death-pe");
    }

    public void load(){

        assetManager.load("pack.atlas", TextureAtlas.class);
        ParticleEffectLoader.ParticleEffectParameter parameter  =   new ParticleEffectLoader.ParticleEffectParameter();

        parameter.atlasFile =  "pack.atlas";
        for (String resource : particlesList) {
            assetManager.load(particlesPath + resource, ParticleEffect.class, parameter);
        }
        for (String resource : soundsList) {
            assetManager.load(soundsPath + resource + audioExtension, Sound.class);
        }
        for (String resource : musicList) {
            assetManager.load(musicPath + resource + audioExtension, Music.class);
        }
        for (String resource : spinesList) {
            com.kirakossian.island.resources.loaders.SpineAnimationLoader.SpineAnimationLoaderParameter spineAnimationLoaderParameter = new SpineAnimationLoader.SpineAnimationLoaderParameter();
            spineAnimationLoaderParameter.parentAtlas = spinesPath+resource+"/"+resource+".atlas";
            assetManager.load(spinesPath + resource + "/" + resource + ".json", SpineAnimation.class, spineAnimationLoaderParameter);
        }
    }
    public void init() {
        textureAtlas    =   assetManager.get( "pack.atlas", TextureAtlas.class);
        for (String resource : particlesList) {
            particles.put(resource, assetManager.get(particlesPath + resource, ParticleEffect.class));
        }
        for (String resource : soundsList) {
            sounds.put(resource, assetManager.get(soundsPath + resource + audioExtension, Sound.class));
        }
        for (String resource : musicList) {
            musics.put(resource, assetManager.get(musicPath + resource + audioExtension, Music.class));
        }
        for (String resource : spinesList) {
            spineAnimations.put(resource, assetManager.get(spinesPath + resource + "/" + resource + ".json", SpineAnimation.class));
        }
    }
    public TextureRegion getTextureRegion (String name) {
        return textureAtlas.findRegion(name);
    }
    public TextureAtlas.AtlasRegion getAtlasRegion (String name) {
        return textureAtlas.findRegion(name);
    }
    public ParticleEffect getParticleEffect (String name) {
        return particles.get(name);
    }

    public AssetManager getAssetManager() {
        return assetManager;
    }
    public Array<TextureAtlas.AtlasRegion> getSpriteFrames(String name){
        Array<TextureAtlas.AtlasRegion> runningFrames = textureAtlas.findRegions(name);
        return runningFrames;

    }

    public Music getMusic(String name) {
        return musics.get(name);
    }

    public Sound getSound(String name) {
        return sounds.get(name);
    }

    public SpineAnimation getSpineAnimations(String name) {
        return spineAnimations.get(name);
    }

    public SkeletonMeshRenderer getSkeletonRenderer() {
        return skeletonMeshrenderer;
    }

    public static class SpineAnimation implements Disposable {
        public SkeletonJson skeletonJson;
        public SkeletonData skeletonData;
        public AnimationStateData animationStateData;
        public TextureAtlas spineAtlas;

        @Override
        public void dispose () {
            if (spineAtlas != null) spineAtlas.dispose();
        }
    }
}
