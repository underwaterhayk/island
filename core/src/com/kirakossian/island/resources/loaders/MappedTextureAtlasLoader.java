package com.kirakossian.island.resources.loaders;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.SynchronousAssetLoader;
import com.badlogic.gdx.assets.loaders.TextureLoader.TextureParameter;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.TextureAtlasData;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.TextureAtlasData.Page;
import com.badlogic.gdx.utils.Array;
import com.kirakossian.island.resources.MappedTextureAtlas;

public class MappedTextureAtlasLoader extends SynchronousAssetLoader<MappedTextureAtlas, MappedTextureAtlasLoader.MappedTextureAtlasParameter> {

	public MappedTextureAtlasLoader(FileHandleResolver resolver) {
		super(resolver);
	}

	TextureAtlasData data;

	@Override
	public MappedTextureAtlas load (AssetManager assetManager, String fileName, FileHandle file, MappedTextureAtlasParameter parameter) {
		for (Page page : data.getPages()) {
			Texture texture = assetManager.get(page.textureFile.path().replaceAll("\\\\", "/"), Texture.class);
			page.texture = texture;
		}

		MappedTextureAtlas mappedTextureAtlas = new MappedTextureAtlas(data);
		return mappedTextureAtlas;
	}

	@Override
	public Array<AssetDescriptor> getDependencies (String fileName, FileHandle atlasFile, MappedTextureAtlasParameter parameter) {
		FileHandle imgDir = atlasFile.parent();

		if (parameter != null)
			data = new TextureAtlasData(atlasFile, imgDir, parameter.flip);
		else {
			data = new TextureAtlasData(atlasFile, imgDir, false);
		}

		Array<AssetDescriptor> dependencies = new Array();
		for (Page page : data.getPages()) {
			TextureParameter params = new TextureParameter();
			params.format = page.format;
			params.genMipMaps = page.useMipMaps;
			params.minFilter = page.minFilter;
			params.magFilter = page.magFilter;
			dependencies.add(new AssetDescriptor(page.textureFile, Texture.class, params));
		}
		return dependencies;
	}

	static public class MappedTextureAtlasParameter extends AssetLoaderParameters<MappedTextureAtlas> {
		/** whether to flip the texture atlas vertically **/
		public boolean flip = false;

		public MappedTextureAtlasParameter () {
		}

		public MappedTextureAtlasParameter (boolean flip) {
			this.flip = flip;
		}
	}
}
