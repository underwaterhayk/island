package com.kirakossian.island.resources.loaders;

import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.SynchronousAssetLoader;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.esotericsoftware.spine.AnimationStateData;
import com.esotericsoftware.spine.SkeletonData;
import com.esotericsoftware.spine.SkeletonJson;
import com.kirakossian.island.resources.GameResourceManager;
import com.kirakossian.island.resources.MappedTextureAtlas;


public class SpineAnimationLoader extends SynchronousAssetLoader<GameResourceManager.SpineAnimation, SpineAnimationLoader.SpineAnimationLoaderParameter> {

	public SpineAnimationLoader(FileHandleResolver resolver) {
		super(resolver);
	}

	@Override
	public GameResourceManager.SpineAnimation load (AssetManager am, String fileName, FileHandle file, SpineAnimationLoaderParameter param) {
		GameResourceManager.SpineAnimation spineAnimation = new GameResourceManager.SpineAnimation();

		if (param != null && param.parentAtlas != null) {
			spineAnimation.spineAtlas = am.get(param.parentAtlas, MappedTextureAtlas.class);

			SkeletonJson skeletonJson = new SkeletonJson(spineAnimation.spineAtlas);
			SkeletonData skeletonData = skeletonJson.readSkeletonData(file);
			AnimationStateData animationData = new AnimationStateData(skeletonData);

			spineAnimation.skeletonJson = skeletonJson;
			spineAnimation.skeletonData = skeletonData;
			spineAnimation.animationStateData = animationData;
		}
		return spineAnimation;
	}

	@Override
	public Array<AssetDescriptor> getDependencies (String fileName, FileHandle file, SpineAnimationLoaderParameter param) {
		Array<AssetDescriptor> deps = null;
		if (param != null && param.parentAtlas != null) {
			deps = new Array();
			deps.add(new AssetDescriptor<MappedTextureAtlas>(param.parentAtlas, MappedTextureAtlas.class));
		} else {
			throw new GdxRuntimeException("Spine animation must have parent atlas param set.");
		}
		return deps;
	}


	public static class SpineAnimationLoaderParameter extends AssetLoaderParameters<GameResourceManager.SpineAnimation> {
		public String parentAtlas;
	}
}
