package com.kirakossian.island.logic.data;

import com.badlogic.gdx.math.Vector2;
import com.kirakossian.island.logic.actors.Land;
import com.kirakossian.island.logic.actors.Portal;
import com.sun.org.apache.bcel.internal.generic.LADD;

public class EnemyData {
    private final Portal portal;

    private final Vector2 destination;

    private final float speed;

    public EnemyData(Portal portal, float speed) {
        this.portal = portal;
        this.destination = Land.enemyDestinationPointtMap.get(portal.getPortalData().getDirectionState());
        this.speed = speed;
    }

    public Portal getPortal() {
        return portal;
    }

    public Vector2 getDestination() {
        return destination;
    }

    public float getSpeed() {
        return speed;
    }
}
