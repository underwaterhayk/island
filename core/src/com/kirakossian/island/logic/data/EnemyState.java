package com.kirakossian.island.logic.data;

public enum EnemyState {
    WALKING_AIR, WALKING_LAND, ON_DESTINATION, DEAD
}
