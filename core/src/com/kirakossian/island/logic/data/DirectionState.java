package com.kirakossian.island.logic.data;

public enum DirectionState {
    UP_RIGHT, UP_LEFT, DOWN_RIGHT, DOWN_LEFT
}
