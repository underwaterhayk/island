package com.kirakossian.island.logic.data;

import com.badlogic.gdx.math.Vector2;
import com.kirakossian.island.logic.actors.Land;

public class PortalData {

    // This coordinates are in land
    private final Vector2 spawnCoords;

    private  Vector2 middleCoords;
    private  DirectionState directionState;


    public PortalData( DirectionState directionState) {
        this.directionState =   directionState;
        this.spawnCoords =  Land.directionStartPointtMap.get(directionState);
        this.middleCoords = Land.directionPointMap.get(directionState);
    }

    public Vector2 getSpawnCoords() {
        return spawnCoords;
    }

    public Vector2 getMiddleCoords() {
        return middleCoords;
    }

    public DirectionState getDirectionState() {
        return directionState;
    }
}
