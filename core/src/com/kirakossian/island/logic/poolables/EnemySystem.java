package com.kirakossian.island.logic.poolables;

import com.badlogic.gdx.utils.Pool;
import com.kirakossian.island.logic.actors.Enemy;

public class EnemySystem {

    private Pool<Enemy> enemyPool = new Pool<Enemy>() {
        @Override
        protected Enemy newObject() {
            return new Enemy();
        }

        @Override
        protected void reset(Enemy object) {
            super.reset(object);
        }
    };

    public EnemySystem() {
    }

    public Enemy obtain() {
        return enemyPool.obtain();
    }

    public void free(Enemy enemy) {
        enemyPool.free(enemy);
    }
}
