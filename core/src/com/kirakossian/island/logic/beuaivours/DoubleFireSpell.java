package com.kirakossian.island.logic.beuaivours;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.kirakossian.island.Facade;
import com.kirakossian.island.logic.actors.spellActors.FireBall;
import com.kirakossian.island.stages.GameStage;

public class DoubleFireSpell extends Spell {


    private  final int speed    =   400;
    @Override
    public float getDurration() {
        return 1f;
    }

    @Override
    public String getRegion() {
        return "fire-spel";
    }

    @Override
    public void start() {
        super.start();
        gameStage.getHero().useDoubleFireSpellEffect();
        FireBall fireBall =   new FireBall(speed);
        gameStage.getActionActor().addAction(Actions.sequence(
                Actions.delay(0.25f),
                Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        FireBall fireBall1 =   new FireBall(speed);
                        end();
                    }
                })
        ));
    }
}
