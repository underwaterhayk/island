package com.kirakossian.island.logic.beuaivours;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.kirakossian.island.Facade;
import com.kirakossian.island.logic.Hero;
import com.kirakossian.island.logic.actors.spellActors.FireBall;

public class FireSpell extends Spell {
    private  final int speed    =   400;
    @Override
    public float getDurration() {
        return 0.5f;
    }

    @Override
    public String getRegion() {
        return "fire-spel";
    }

    @Override
    public void start() {
        super.start();
        FireBall   fireBall =   new FireBall(speed);
        gameStage.getHero().useFireSpellEffect();
        gameStage.getActionActor().addAction(Actions.sequence(
                Actions.delay(0.5f),
                Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        end();
                    }
                })
        ));
    }
}
