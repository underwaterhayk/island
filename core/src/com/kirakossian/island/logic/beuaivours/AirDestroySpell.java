package com.kirakossian.island.logic.beuaivours;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.kirakossian.island.Facade;
import com.kirakossian.island.stages.GameStage;

public class AirDestroySpell extends Spell {
    @Override
    public float getDurration() {
        return 25;
    }
    @Override
    public String getRegion() {
        return "fire-spel";
    }

    @Override
    public void start() {
        super.start();
        gameStage.getHero().useAirSpellEffect();
        gameStage.getActionActor().addAction(Actions.sequence(
                Actions.delay(0.2f),
                Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        end();
                    }
                })
        ));
    }
}
