package com.kirakossian.island.logic.beuaivours;

import com.badlogic.gdx.utils.Array;

public class BehaivourManager {
    Array<Behaivour> behaivours =   new Array<Behaivour>();

    public BehaivourManager() {

    }
    public void addBehaivour(Behaivour behaivour){
        if(behaivours.size==0){
            behaivour.start();
        }
        behaivours.add(behaivour);
    }
    public void removeBehaivour(){
        behaivours.removeIndex(0);
        if(behaivours.size>0){
            behaivours.get(0).start();
        }
    }

    public void reset() {
        behaivours.clear();
    }
}
