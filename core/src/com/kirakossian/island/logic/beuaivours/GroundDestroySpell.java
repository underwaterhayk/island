package com.kirakossian.island.logic.beuaivours;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.kirakossian.island.Facade;
import com.kirakossian.island.logic.actors.Enemy;
import com.kirakossian.island.logic.data.EnemyState;
import com.kirakossian.island.stages.GameStage;

public class GroundDestroySpell extends Spell {
    @Override
    public float getDurration() {
        return 20;
    }
    @Override
    public String getRegion() {
        return "fire-spel";
    }

    @Override
    public void start() {
        super.start();

        gameStage.getActionActor().addAction(Actions.sequence(
                Actions.delay(0.02f),
                Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        gameStage.getHero().useGroundSpellEffect();
                        gameStage.getLand().groundExplossion();
                        end();
                    }
                })
        ));

    }
}
