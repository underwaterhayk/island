package com.kirakossian.island.logic.beuaivours;

import com.kirakossian.island.logic.Hero;
import com.kirakossian.island.logic.data.DirectionState;

public class MoveBehaivour extends Behaivour {
    private  Hero hero;
    private  DirectionState direction;

    public MoveBehaivour(DirectionState direction, Hero hero) {
        super();
        this.direction  =   direction;
        this.hero       =   hero;
    }

    @Override
    public void start() {
        hero.setDirection(direction);
    }

    @Override
    public void end() {
        super.end();
    }
}
