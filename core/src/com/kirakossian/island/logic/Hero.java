package com.kirakossian.island.logic;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.esotericsoftware.spine.AnimationState;
import com.kirakossian.island.Facade;
import com.kirakossian.island.logic.actors.Enemy;
import com.kirakossian.island.logic.actors.ParticleActor;
import com.kirakossian.island.logic.actors.SpineActor;
import com.kirakossian.island.logic.beuaivours.GroundDestroySpell;
import com.kirakossian.island.logic.data.DirectionState;
import com.kirakossian.island.logic.data.EnemyState;
import com.kirakossian.island.stages.GameStage;

import static com.kirakossian.island.Facade.dispose;

public class Hero extends Group {

    private final Rectangle bounds;
    private final GameStage gameStage;
    private SpineActor heroSpine;

    public static DirectionState direction = DirectionState.UP_RIGHT;
    private Texture pixmapTexture    =   null;
    private boolean shield;
    private ParticleActor shieldActor;

    public Hero(GameStage gameStage) {

        shieldActor =   new ParticleActor("shield");


        heroSpine = new SpineActor("hero-right-front");
        heroSpine.setScale(0.4f);
        addActor(heroSpine);
        setWidth(30);
        setHeight(30);
        setPosition(gameStage.getLand().getCenterPos().x, gameStage.getLand().getCenterPos().y);
        this.gameStage =   gameStage;

        bounds  =   new Rectangle((int)getX()-getWidth()/2, (int)getY(), (int)getWidth(), (int)getHeight());
        initBlackPixel();

    }

    private void initBlackPixel() {
        Pixmap pixmap = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
        pixmap.setColor(Color.BLACK);
        pixmap.fillRectangle(0, 0, 1, 1);
        pixmapTexture = new Texture(pixmap, Pixmap.Format.RGBA8888, false);

        pixmap.dispose();

    }
    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
//        if(pixmapTexture!=null)
//        batch.draw(pixmapTexture,bounds.x, bounds.y, bounds.width, bounds.height);
    }

    public void setDirection(DirectionState direction) {
        this.direction = direction;
        if (direction == DirectionState.UP_RIGHT) {
            heroSpine.setSkeleton("hero-right-front");
            heroSpine.flipX(true);
        } else if (direction == DirectionState.UP_LEFT) {
            heroSpine.setSkeleton("hero-right-front");
            heroSpine.flipX(false);
        } else if (direction == DirectionState.DOWN_LEFT) {
            heroSpine.setSkeleton("hero-right-front");
            heroSpine.flipX(false);
        } else if (direction == DirectionState.DOWN_RIGHT) {
            heroSpine.setSkeleton("hero-right-front");
            heroSpine.flipX(true);
        }

    }




    private void addKillEnemyParticle(Enemy enemy) {
        ParticleEffect particleEffect   =   new ParticleEffect(Facade.getGame().getRm().getParticleEffect("zombie-death-pe"));
        ParticleActor particleActor =   new ParticleActor(particleEffect,1);
        particleActor.start();
        particleActor.setPosition(enemy.getX(), enemy.getY());
        gameStage.getLand().addActor(particleActor);
    }

    private void killGroundEnemies() {
        GameStage  gameStage = (GameStage) Facade.getGame().getCurrentScreen().getStage();
        for (Enemy enemy : gameStage.getLand().getEnemiesByState(EnemyState.WALKING_LAND)){
            addKillEnemyParticle(enemy);
            gameStage.getLand().killEnemy(enemy);

        }
    }



    private void killAirEnemies() {
        final GameStage  gameStage = (GameStage) Facade.getGame().getCurrentScreen().getStage();
        for (final Enemy enemy : gameStage.getLand().getEnemiesByState(EnemyState.WALKING_AIR)){
            ParticleEffect particleEffect   =   new ParticleEffect(Facade.getGame().getRm().getParticleEffect("air-attack-particle"));
            final ParticleActor particleActor =   new ParticleActor(particleEffect,1);

            particleActor.setPosition(gameStage.getLand().getCenterPos().x, gameStage.getLand().getCenterPos().y);
            particleActor.start();
            gameStage.getLand().addActor(particleActor);
            particleActor.addAction(Actions.sequence(
                    Actions.moveTo(gameStage.getLand().getCenterPos().x, gameStage.getLand().getCenterPos().y+250, 0.3f,Interpolation.pow2),
                    Actions.moveTo(enemy.getX(), enemy.getY(), (float) (calculateDistanceBetweenPoints(particleActor.getX(), particleActor.getY(), enemy.getX(), enemy.getY())/700f),Interpolation.pow2InInverse),
                    Actions.run(new Runnable() {
                        @Override
                        public void run() {
                            addKillEnemyParticle(enemy);
                            gameStage.getLand().killEnemy(enemy);
                            particleActor.dispose();
                            particleActor.remove();
                        }
                    })
            ));
        }
    }
    public double calculateDistanceBetweenPoints(float x1, float y1, float x2, float y2){
        return Math.sqrt((y2 - y1) * (y2 - y1) + (x2 - x1) * (x2 - x1));
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        checkCollisionWithEnemy();
    }

    private void checkCollisionWithEnemy() {
        for (Enemy enemy : gameStage.getLand().getEnemiesByState(EnemyState.WALKING_LAND)){
            if(enemy.getBounds().overlaps(bounds)){
                if(shield){
                    gameStage.getLand().killEnemy(enemy);
                    usetShield();
                }else{
                    enemy.attack();
                }
            }
        }
    }

    private void usetShield() {
        shield  =   false;
        shieldActor.remove();
        ParticleActor removeShieldActor =   new ParticleActor("shieldRemove");
        addActor(removeShieldActor);
        removeShieldActor.setY(50);
        removeShieldActor.start();

    }

    public void setShield(){
        if(shield)return;
        shield =   true;
        shieldActor.setY(50);
        shieldActor.start();
        addActor(shieldActor);
    }

    public void useFireSpellEffect() {
        heroSpine.setAnimation("shoot", false);
        heroSpine.addAnimation("idle", true);
    }
    public void useDoubleFireSpellEffect() {
        heroSpine.setAnimation("shoot", false);
        heroSpine.addAnimation("idle", true);
    }

    public void useAirSpellEffect() {
        heroSpine.removeAnimationStateListener();
        AnimationState.AnimationStateAdapter animationStateListener = new AnimationState.AnimationStateAdapter() {
            @Override
            public void start(AnimationState.TrackEntry entry) {
                super.start(entry);
            }

            @Override
            public void end(AnimationState.TrackEntry entry) {
                super.end(entry);
                killAirEnemies();
                heroSpine.addAnimation("idle", true);
                heroSpine.removeAnimationStateListener();
            }
        };
        heroSpine.setAnimationListener(animationStateListener);
        heroSpine.setAnimation("shoot");


    }

    public void useGroundSpellEffect() {
        heroSpine.removeAnimationStateListener();
        AnimationState.AnimationStateAdapter animationStateListener = new AnimationState.AnimationStateAdapter() {
            @Override
            public void start(AnimationState.TrackEntry entry) {
                super.start(entry);
            }

            @Override
            public void end(AnimationState.TrackEntry entry) {
                super.end(entry);

            }

            @Override
            public void complete(AnimationState.TrackEntry entry) {
                super.complete(entry);
                killGroundEnemies();
                heroSpine.addAnimation("idle", true);
                heroSpine.removeAnimationStateListener();
            }
        };
        heroSpine.setAnimationListener(animationStateListener);
        heroSpine.setAnimation("shoot");

    }
}
