package com.kirakossian.island.logic.enemy;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.kirakossian.island.logic.actors.Land;
import com.kirakossian.island.logic.enemy.data.BotSessionData;
import com.kirakossian.island.logic.enemy.data.BotSpawnData;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class EnemyGenerator {

    private final Land land;

    private final List<BotSessionData> sessions = new ArrayList<BotSessionData>();

    private final double sessionDelay;

    private int curSession = 0;

    private int curBot = 0;

    private boolean generate = false;

    private float curDelta = 0f;

    private boolean sessionStarted = false;

    public EnemyGenerator(Land land) {
        this.land = land;

        FileHandle handle = Gdx.files.internal("json/data.json");
        JSONObject jsonObject = new JSONObject(handle.readString());

        sessionDelay = jsonObject.getDouble("session_delay");

        JSONArray sessionsArray = jsonObject.getJSONArray("sessions");

        for (int i = 0; i < sessionsArray.length(); i++) {
            JSONObject sessionObject = sessionsArray.getJSONObject(i);
            BotSessionData botSessionData = new BotSessionData();
            sessions.add(botSessionData);

            JSONArray sessionBots = sessionObject.getJSONArray("session_bots");
            for (int j = 0; j < sessionBots.length(); j++) {
                JSONObject sessionBotObject = sessionBots.getJSONObject(j);

                BotSpawnData spawnData = new BotSpawnData(sessionBotObject.getInt("portal"), sessionBotObject.getDouble("delay"));
                botSessionData.addSpawn(spawnData);
            }
        }
    }

    /**
     * Call this method for resetting session when you want to start the game again
     * This method should be called before startGenerating is called
     */
    public void resetSession() {
        curBot = 0;
        curSession = 0;
        curDelta = 0;
        sessionStarted = false;
    }

    /**
     * Start generating again
     * If you want to unpause just call this method, but if you want to start over call resetSession first
     */
    public void startGenerating() {
        generate = true;
    }

    /**
     * Stop generating. call this when you want to pause
     */
    public void stopGenerating() {
        generate = false;
    }

    public void step(float delta) {
        boolean deltaResetted = false;
        if(generate) {
            BotSessionData session = sessions.get(curSession);
            BotSpawnData bot = session.getSpawns().get(curBot);
            if(sessionStarted) {
                if(curDelta >= bot.getDelay()) {
                    land.spawnBotOnPortal(bot.getPortal());

                    if(curBot < session.getSpawns().size() - 1) {
                        // change bot
                        curBot++;
                    }
                    else {
                        // change session
                        curBot = 0;
                        sessionStarted = false;
                        if(curSession < sessions.size() - 1) {
                            curSession++;
                            curDelta = 0f;
                            deltaResetted = true;
                        }
                        else {
                            // All sessions ended
                            resetSession();
                            stopGenerating();
                        }
                    }
                }
            }
            else {
                if(curDelta >= sessionDelay) {
                    sessionStarted = true;
                    curDelta = 0f;
                    deltaResetted = true;
                }
            }

            if(!deltaResetted) {
                curDelta += delta;
            }
        }
    }

    private boolean checkCurBot() {
        return curDelta >= sessions.get(curSession).getSpawns().get(curBot).getDelay();
    }
}
