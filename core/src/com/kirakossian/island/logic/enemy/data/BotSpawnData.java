package com.kirakossian.island.logic.enemy.data;

public class BotSpawnData {

    private final int portal;

    private final double delay;

    public BotSpawnData(int portal, double delay) {
        this.portal = portal;
        this.delay = delay;
    }

    public int getPortal() {
        return portal;
    }

    public double getDelay() {
        return delay;
    }
}
