package com.kirakossian.island.logic.enemy.data;

import java.util.ArrayList;
import java.util.List;

public class BotSessionData {

    private final List<BotSpawnData> spawns = new ArrayList<BotSpawnData>();

    public List<BotSpawnData> getSpawns() {
        return spawns;
    }

    public void addSpawn(BotSpawnData spawnData) {
        spawns.add(spawnData);
    }
}
