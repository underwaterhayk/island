package com.kirakossian.island.logic.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Pool;
import com.esotericsoftware.spine.AnimationState;
import com.kirakossian.island.Facade;
import com.kirakossian.island.logic.data.DirectionState;
import com.kirakossian.island.logic.data.EnemyData;
import com.kirakossian.island.logic.data.EnemyState;

public class Enemy extends Group implements Pool.Poolable {


    private final SpineActor enemySpine;
    private EnemyData enemyData;

    private EnemyState enemyState;

    // Cache for not creating in act
    private Vector2 direction = new Vector2();

    private boolean moving = false;

    private Rectangle bounds;
    private Texture pixmapTexture;

    public Enemy() {
        enemySpine = new SpineActor("zombie-right-front");
        enemySpine.setSpeed(3);
        enemySpine.setScale(0.4f);

        enemySpine.setAnimation("walk");

        addActor(enemySpine);

        setWidth(30);
        setHeight(70);

        bounds  =   new Rectangle((int)getX()-getWidth()/2, (int)getY(), (int)getWidth(), (int)getHeight());
        initBlackPixel();

    }
    private void initBlackPixel() {
        Pixmap pixmap = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
        pixmap.setColor(Color.BLACK);
        pixmap.fillRectangle(0, 0, 1, 1);
        pixmapTexture = new Texture(pixmap, Pixmap.Format.RGBA8888, false);

        pixmap.dispose();

    }
    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
//        if(pixmapTexture!=null)
//            batch.draw(pixmapTexture,bounds.x, bounds.y, bounds.width, bounds.height);
    }


    @Override
    public void reset() {
        setX(0);
        setY(0);
        enemyData = null;
        enemyState = null;
        direction.set(0,0);
        moving = false;
    }

    public void init(EnemyData enemyData) {
        this.enemyData = enemyData;
        setCoord(enemyData.getPortal().getPortalData().getSpawnCoords());
        enemyState = EnemyState.WALKING_AIR;

        moving = true;
        initDirection();


        if (enemyData.getPortal().getPortalData().getDirectionState() == DirectionState.UP_RIGHT) {
            enemySpine.setAnimation("walk");
            enemySpine.flipX(false);
        } else if (enemyData.getPortal().getPortalData().getDirectionState() == DirectionState.UP_LEFT) {
            enemySpine.setAnimation("walk");
            enemySpine.flipX(true);
        } else if (enemyData.getPortal().getPortalData().getDirectionState() == DirectionState.DOWN_LEFT) {
            enemySpine.setAnimation("walk");
            enemySpine.flipX(false);
        } else if (enemyData.getPortal().getPortalData().getDirectionState() == DirectionState.DOWN_RIGHT) {
            enemySpine.setAnimation("walk");
            enemySpine.flipX(true);
        }


    }

    public EnemyState getEnemyState() {
        return enemyState;
    }

    private void initDirection() {
        Vector2 destination = getDestination();
        direction.set(destination.x - getX(), destination.y - getY()).nor();
    }

    public Rectangle getBounds(){
        return bounds;
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        if(moving) {
            float change = delta * enemyData.getSpeed();
            move(change);
        }
    }

    private void move(float change) {
        Vector2 destination = getDestination();
        setX(getX() + change * direction.x);
        setY(getY() + change * direction.y);

        bounds.x    =   getX()-getWidth()/2;
        bounds.y    =   getY();

        changeState(destination);
    }

    private void changeState(Vector2 destination) {
        if(!MathUtils.isEqual(Math.signum(destination.x - getX()), Math.signum(direction.x)) && !MathUtils.isEqual(Math.signum(destination.y - getY()), Math.signum(direction.y))) {
            if(EnemyState.WALKING_AIR.equals(enemyState)){

                enemyState  =   EnemyState.WALKING_LAND;

                enemySpine.addAnimation("walk", true);

            }else{
                enemyState  =   EnemyState.ON_DESTINATION;

            }


            initDirection();
            setCoord(destination);

            moving = !EnemyState.ON_DESTINATION.equals(enemyState);
        }
    }

    private void setCoord(Vector2 coord) {
        setX(coord.x);
        setY(coord.y);
    }

    private Vector2 getDestination() {
        if(EnemyState.WALKING_AIR.equals(enemyState)) {
            return enemyData.getPortal().getPortalData().getMiddleCoords();
        }
        return enemyData.getDestination();
    }

    public EnemyData getEnemyData() {
        return enemyData;
    }

    public void die() {
        remove();
        enemyState  =   EnemyState.DEAD;

    }

    public void attack() {
        enemySpine.removeAnimationStateListener();
        AnimationState.AnimationStateAdapter animationStateListener = new AnimationState.AnimationStateAdapter() {
            @Override
            public void start(AnimationState.TrackEntry entry) {
                super.start(entry);
            }

            @Override
            public void end(AnimationState.TrackEntry entry) {
                super.end(entry);

            }

            @Override
            public void complete(AnimationState.TrackEntry entry) {
                super.complete(entry);
                if(enemyState==EnemyState.ON_DESTINATION) {
                    Facade.getGame().gameOver();
                }
            }
        };
        enemySpine.setAnimationListener(animationStateListener);
        enemySpine.addAnimation("attack", true);
    }
}
