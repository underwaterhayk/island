package com.kirakossian.island.logic.actors.spellActors;

import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.kirakossian.island.Facade;
import com.kirakossian.island.logic.Hero;
import com.kirakossian.island.logic.actors.Enemy;
import com.kirakossian.island.logic.actors.Land;
import com.kirakossian.island.logic.actors.ParticleActor;
import com.kirakossian.island.logic.data.EnemyState;
import com.kirakossian.island.stages.GameStage;


public class FireBall extends Group {
    public final static int WIDTH     =   15;
    public final static int HEIGHT    =   15;
    private final Rectangle bounds;
    private final ParticleActor sttackParticleActor;
    private  GameStage gameStage;


    private Vector2 direction = new Vector2();
    private float speed;
    boolean preRemove   =   false;

    public FireBall(int speed) {
        this.speed  =   speed;

        ParticleEffect particleEffect   =   new ParticleEffect(Facade.getGame().getRm().getParticleEffect("groud-attack-pe"));
        sttackParticleActor     =   new ParticleActor(particleEffect,1);
        sttackParticleActor.start();
        addActor(sttackParticleActor);

//        Image image =   new Image(Facade.getGame().getRm().getTextureRegion("circle-wave"));
//        image.setWidth(WIDTH);
//        image.setHeight(HEIGHT);
//        addActor(image);

        setWidth(WIDTH);
        setHeight(HEIGHT);

        bounds  =   new Rectangle((int)getX(), (int)getY(), (int)getWidth(), (int)getHeight());

        gameStage =   (GameStage)Facade.getGame().getCurrentScreen().getStage();
        gameStage.getLand().addActor(this);
        setX(gameStage.getHero().getX()+gameStage.getHero().getWidth()/2-getWidth()/2);
        setY(gameStage.getHero().getY()+70);
        initDirection();
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        move(delta*speed);
    }
    private void initDirection() {

        Vector2 destination = Land.directionPointMap.get(Hero.direction);
        direction.set(destination.x - getX(), destination.y - getY()).nor();
    }

    private void move(float change) {
        if(preRemove)return;
        setX(getX() + change * direction.x);
        setY(getY() + change * direction.y);
        bounds.x    =   getX();
        bounds.y    =   getY();

        checkCollisionWithEnemy();
    }

    private void checkCollisionWithEnemy() {
        for (Enemy enemy : gameStage.getLand().getEnemiesOnDirectionOnGround()){
            if(enemy.getBounds().overlaps(bounds)){
                gameStage.getLand().killEnemy(enemy);
                addFireCollision();
                preRemove   =   true;
                sttackParticleActor.remove();
                addAction(Actions.addAction(Actions.sequence(
                        Actions.delay(0.5f),
                        Actions.run(new Runnable() {
                            @Override
                            public void run() {
                                dispose();
                            }
                        })
                )));
            }
        }
    }

    private void addFireCollision() {
        ParticleActor particleActor =   new ParticleActor("fireCollision");
        particleActor.start();
        addActor(particleActor);
    }

    private void dispose() {
        remove();
    }
}
