package com.kirakossian.island.logic.actors;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.kirakossian.island.Facade;

/**
 * Created by kirakos on 1/6/18.
 */
public class ParticleActor extends Actor {
    private final float duration;
    private ParticleEffect particleEffect;
    private float passedTime;
    public ParticleActor(String name) {
        this(name, 1);
    }
    public ParticleActor(ParticleEffect particleEffect, float scale) {
        this.particleEffect =   particleEffect;
        particleEffect.scaleEffect(scale);
        duration    =   calculateTerminationTime();
        setVisible(false);
    }
    public ParticleActor(String name, float scale) {
        particleEffect      =   Facade.getGame().getRm().getParticleEffect(name);
        particleEffect.scaleEffect(scale);
        duration    =   calculateTerminationTime();
        setVisible(false);
    }

    @Override
    public void setPosition(float x, float y) {
        super.setPosition(x, y);
        particleEffect.setPosition(x,y);
    }
    @Override
    public void act(float delta) {
        super.act(delta);
        particleEffect.update(delta);
        if(isVisible()) {
            passedTime+=delta;
            if(duration>0 && passedTime>duration){
                passedTime = 0;
                setVisible(false);
                dispose();
            }
        }

    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        if(isVisible()) {
            particleEffect.setPosition(this.getX(),this.getY());
            particleEffect.draw(batch);
        }
    }

    private float calculateTerminationTime() {
        float maxTime = 0;
        boolean continuous = false;

        for (ParticleEmitter emitter : particleEffect.getEmitters()) {
            float dur = (emitter.getDelay().getLowMax() + emitter.getDuration().getLowMax() + emitter.getLife().getHighMax()) / 1000f;
            if (dur > maxTime) maxTime = dur;
            if (emitter.isContinuous()) {
                continuous = true;
            }
        }
        float time = maxTime;

        if(continuous) {
            time = -1;
        }
        return time;
    }

    public void dispose(){
        this.remove();
    }

    public void start() {
        setVisible(true);
        particleEffect.start();


    }
    public void decreaseEmmiting(){
        particleEffect.getEmitters().first().setMaxParticleCount(100);
    }
    public void reset(){
        particleEffect.reset();
    }


    public void changeColor(){
        float [] a  =   new  float[2];
        a[0]    =   0.1f;
        a[1]    =   0.9f;
        for (int i = 0; i < particleEffect.getEmitters().size; i++) {
            particleEffect.getEmitters().get(i).getTint().setColors(a);
        }
    }
}

