package com.kirakossian.island.logic.actors;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.esotericsoftware.spine.Skeleton;
import com.esotericsoftware.spine.Slot;
import com.esotericsoftware.spine.attachments.Attachment;
import com.esotericsoftware.spine.attachments.MeshAttachment;
import com.esotericsoftware.spine.attachments.RegionAttachment;

/**
 * Created by ZeppLondon on 11/1/2016.
 */
public class SpineUtils {
    private static Array<float[]> getVerticesSlots(Skeleton skeleton){
        skeleton.updateWorldTransform();

        float minX = Float.MAX_VALUE, minY = Float.MAX_VALUE;
        float maxX = Float.MIN_VALUE, maxY = Float.MIN_VALUE;
        Array<float[]> vericesSlots = new Array<float[]>();
        for (int i = 0, n = skeleton.getSlots().size; i < n; i++) {
            Slot slot = skeleton.getSlots().get(i);
            Attachment attachment = slot.getAttachment();
            if (attachment == null) continue;
            if (!(attachment instanceof RegionAttachment || attachment instanceof MeshAttachment)) continue;
            float[] vertices = new float[4];
            if ((attachment instanceof RegionAttachment)) {
                RegionAttachment region = (RegionAttachment) attachment;
                region.updateWorldVertices(slot, false);
                vertices = region.getWorldVertices();
            }
            if ((attachment instanceof MeshAttachment)) {
                MeshAttachment region = (MeshAttachment) attachment;
                region.updateWorldVertices(slot, false);
                vertices = region.getWorldVertices();
            }

            vericesSlots.add(vertices);
        }
        return vericesSlots;
    }

    private static float[] getBoundBox(Array<float[]> verticesSlots){
        float minX = Float.MAX_VALUE, minY = Float.MAX_VALUE;
        float maxX = Float.MIN_VALUE, maxY = Float.MIN_VALUE;
        for (int i = 0, n = verticesSlots.size; i < n; i++) {
            float[] vertices = verticesSlots.get(i);

            for (int ii = 0, nn = vertices.length; ii < nn; ii += 5) {
                minX = Math.min(minX, vertices[ii]);
                minY = Math.min(minY, vertices[ii + 1]);
                maxX = Math.max(maxX, vertices[ii]);
                maxY = Math.max(maxY, vertices[ii + 1]);
            }
        }
        float[] bounds = new float[]{minX, maxX, minY, maxY};
        return bounds;
    }

    public static Vector2 getOffset(Skeleton skeleton) {
        float[] boundBox = getBoundBox(getVerticesSlots(skeleton));
        Vector2 offset = new Vector2(boundBox[0], boundBox[2]);
        return offset;
    }

    public static Vector2 getOffset(float[] boundBox) {
        Vector2 offset = new Vector2(boundBox[0], boundBox[2]);
        return offset;
    }

    public static Vector2 getDimensions(float[] boundBox){
        Vector2 dimensions = new Vector2(boundBox[1] - boundBox[0], boundBox[3] - boundBox[2]);
        return dimensions;
    }

    public static float[] getBoundBox(Skeleton skeleton) {
        float[] boundBox = getBoundBox(getVerticesSlots(skeleton));

        return boundBox;
    }

    public static Vector2 getDimensions(Skeleton skeleton){
        float[] boundBox = getBoundBox(getVerticesSlots(skeleton));
        Vector2 dimensions = new Vector2(boundBox[1] - boundBox[0], boundBox[3] - boundBox[2]);
        return dimensions;
    }
}
