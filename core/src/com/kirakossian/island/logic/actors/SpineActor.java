package com.kirakossian.island.logic.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.esotericsoftware.spine.AnimationState;
import com.esotericsoftware.spine.AnimationStateData;
import com.esotericsoftware.spine.Skeleton;
import com.esotericsoftware.spine.SkeletonData;
import com.kirakossian.island.Facade;
import com.kirakossian.island.Island;


/**
 * Created by hayk on 9/29/2016.
 */
public class SpineActor extends Actor {
    private  String name;
    private final Island game;
    private SkeletonData skeletonData;
    private AnimationStateData animationData;
    private Skeleton skeleton;
    private AnimationState animationState;
    private float scaleFactorX;
    private float scaleFactorY;
    private float scale =   1;

    public SpineActor(String name) {
        this.game   =   Facade.getGame();
        this.name   =   name;
        init();
    }
    public void setSkeleton(String name){
        this.name   =   name;
        init();
    }
    private void init() {
        scaleFactorX = 1;
        scaleFactorY = 1;

        skeletonData        =   game.getRm().getSpineAnimations(name).skeletonData;
        animationData       =   new AnimationStateData(skeletonData);
        skeleton            =   new Skeleton(skeletonData);
        animationState      =   new AnimationState(animationData);
        Vector2 dimensions  =   SpineUtils.getDimensions(skeleton);
        setWidth(dimensions.x * scaleFactorX);
        setWidth(dimensions.y * scaleFactorY);
        setScale(scale);
        setAnimation(animationData.getSkeletonData().getAnimations().get(0).getName());
    }
    public void setSpeed(float mul){
        animationState.setTimeScale(mul);
    }
    @Override
    public void act(float delta) {
        super.act(delta);
        animationState.update(delta);
        animationState.apply(skeleton);
    }
    public void setScale(float scale){
        super.setScale(scale);
        this.scale  =   scale;
        skeleton.findBone("root").setScale(this.getScaleX()*scaleFactorX*scale,this.getScaleY()*scaleFactorY*scale);
    }
    @Override
    public void setRotation(float degrees) {
        super.setRotation(degrees);
        skeleton.findBone("root").setRotation(degrees);
    }
    public void flipX(boolean flipx){
        skeleton.setFlipX(flipx);
    }
    @Override
    public float getRotation() {
        return skeleton.findBone("root").getRotation();
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        skeleton.setPosition(this.getX(),this.getY());
        skeleton.updateWorldTransform();
        skeleton.setColor(Color.WHITE);
        int src = batch.getBlendSrcFunc();
        int dst = batch.getBlendDstFunc();
        game.getRm().getSkeletonRenderer().draw((PolygonSpriteBatch) batch, skeleton);
        batch.setBlendFunction(src, dst);
        Gdx.gl.glBlendFuncSeparate( GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA,
                GL20.GL_SRC_ALPHA, GL20.GL_ONE);
    }

    public void setAnimation(String animName){
        setAnimation(animName, true);
    }

    public void setAnimation(String animName, boolean loop){
                animationState.setAnimation(0,animName,loop);
    }
    public void setAnimation(String animName, boolean loop, AnimationState.AnimationStateListener listener){
                animationState.setAnimation(0,animName,loop);
                animationState.addListener(listener);
    }
    public void addAnimation(String animName, boolean loop){
        animationState.addAnimation(0,animName,  loop, 0);
    }

    public void setTimeScale(float scale){
        animationState.setTimeScale(scale);
    }

    public float setTimeScale(){
        return animationState.getTimeScale();
    }

    public void setSkin(String skinName){
        skeleton.setSkin(skinName);
    }

    public void setAnimationListener(AnimationState.AnimationStateListener animationListener){
        animationState.addListener(animationListener);
    }
    public void removeAnimationStateListener(){
        animationState.clearListeners();
    }


}
