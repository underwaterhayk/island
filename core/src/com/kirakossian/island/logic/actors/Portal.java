package com.kirakossian.island.logic.actors;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.kirakossian.island.logic.data.PortalData;

public class Portal extends Group {

    private PortalData portalData;

    public Portal(PortalData portalData) {
        this.portalData = portalData;
    }

    public PortalData getPortalData() {
        return portalData;
    }
}
