package com.kirakossian.island.logic.actors;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.kirakossian.island.Facade;
import com.kirakossian.island.logic.enemy.EnemyGenerator;
import com.kirakossian.island.logic.Hero;
import com.kirakossian.island.logic.data.DirectionState;
import com.kirakossian.island.logic.data.EnemyData;
import com.kirakossian.island.logic.data.EnemyState;
import com.kirakossian.island.logic.data.PortalData;
import com.kirakossian.island.logic.poolables.EnemySystem;
import com.kirakossian.island.stages.GameStage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class Land extends Group {


    public static HashMap<DirectionState, Vector2> directionPointMap = new HashMap<DirectionState, Vector2>()
    {{
        put(DirectionState.UP_RIGHT, new Vector2(313, 470));
        put(DirectionState.UP_LEFT, new Vector2(167, 470));
        put(DirectionState.DOWN_RIGHT, new Vector2(370, 300));
        put(DirectionState.DOWN_LEFT, new Vector2(75, 330));

    }};
    public static HashMap<DirectionState, Vector2> directionStartPointtMap = new HashMap<DirectionState, Vector2>()
    {{
        put(DirectionState.UP_RIGHT, new Vector2(415, 598));
        put(DirectionState.UP_LEFT, new Vector2(104, 586));
        put(DirectionState.DOWN_RIGHT, new Vector2(345, 115));
        put(DirectionState.DOWN_LEFT, new Vector2(122, 122));

    }};
    public static HashMap<DirectionState, Vector2> enemyDestinationPointtMap = new HashMap<DirectionState, Vector2>()
    {{
        put(DirectionState.UP_RIGHT, new Vector2(245, 378));
        put(DirectionState.UP_LEFT, new Vector2(235, 378));
        put(DirectionState.DOWN_RIGHT, new Vector2(245, 368));
        put(DirectionState.DOWN_LEFT, new Vector2(235, 368));

    }};


    private float deltaBufer;

    private Vector2 centerPos;
    private Image landImg;

    private List<PortalData> portalDatas    = new ArrayList<PortalData>();
    private Array<Portal> portals           = new Array<Portal>();

    private List<Enemy> enemies = new ArrayList<Enemy>();

    private EnemySystem enemySystem = new EnemySystem();

    private EnemyGenerator enemyGenerator;

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
    }

    public Land() {
        centerPos   =   new Vector2();
        centerPos.x     =   240;
        centerPos.y     =   373;

        addBuildingsImages();
        addeffects();

        Portal portalNE = new Portal(new PortalData(DirectionState.UP_RIGHT));
        Portal portalNW = new Portal(new PortalData(DirectionState.UP_LEFT));
        Portal portalSE = new Portal(new PortalData(DirectionState.DOWN_RIGHT));
        Portal portalSW = new Portal(new PortalData(DirectionState.DOWN_LEFT));
        addActor(portalNE);
        addActor(portalNW);
        addActor(portalSE);
        addActor(portalSW);
        portals.add(portalNE);
        portals.add(portalNW);
        portals.add(portalSE);
        portals.add(portalSW);

        addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                System.out.println(x+"    "+y);
            }
        });

        enemyGenerator = new EnemyGenerator(this);
        enemyGenerator.startGenerating();
    }

    private void addeffects() {
        addBottomSmoke();
    }

    private void addBottomSmoke() {
        ParticleEffect particleEffect   =   new ParticleEffect(Facade.getGame().getRm().getParticleEffect("fire-smoke"));
        ParticleActor particleActor     =   new ParticleActor(particleEffect,1);
        particleActor.setPosition(directionStartPointtMap.get(DirectionState.DOWN_RIGHT).x, directionStartPointtMap.get(DirectionState.DOWN_RIGHT).y);
        particleActor.start();
        addActor(particleActor);

        ParticleEffect particleEffecLeftt   =   new ParticleEffect(Facade.getGame().getRm().getParticleEffect("fire-smoke-left"));
        ParticleActor particleActorLeft     =   new ParticleActor(particleEffecLeftt,1);
        particleActorLeft.setPosition(directionStartPointtMap.get(DirectionState.DOWN_LEFT).x, directionStartPointtMap.get(DirectionState.DOWN_LEFT).y);
        particleActorLeft.start();
        addActor(particleActorLeft);
    }

    private void addBuildingsImages() {
        landImg =   new Image(Facade.getGame().getRm().getTextureRegion("land"));
        landImg.setWidth(Facade.getGame().getCurrentScreen().getStage().getWidth());

        addActor(landImg);
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        enemyGenerator.step(delta);
    }
    @Override
    public float getWidth() {
        return landImg.getWidth();
    }

    @Override
    public float getHeight() {
        return landImg.getHeight();
    }

    public Vector2 getCenterPos() {
        return centerPos;
    }

    public List<PortalData> getPortalDatas() {
        return portalDatas;
    }

    public List<Enemy> getEnemies() {
        return enemies;
    }
    public List<Enemy> getEnemiesOnDirectionOnGround() {
        List<Enemy> tmpList =   new ArrayList<Enemy>();
        for (Enemy enemy :enemies){
            if((enemy.getEnemyState()== EnemyState.WALKING_LAND || enemy.getEnemyState()== EnemyState.ON_DESTINATION) && enemy.getEnemyData().getPortal().getPortalData().getDirectionState()==Hero.direction){
                tmpList.add(enemy);
            }
        }
        return tmpList;
    }
    public List<Enemy> getEnemiesByState(EnemyState state) {
        List<Enemy> tmpList =   new ArrayList<Enemy>();
        for (Enemy enemy :enemies){
            if(enemy.getEnemyState()==state){
                tmpList.add(enemy);
            }
        }
        return tmpList;
    }

    public void spawnBotWithData(EnemyData enemyData) {
        Enemy enemy = enemySystem.obtain();

        enemy.init(enemyData);

        enemies.add(enemy);

        addActor(enemy);
    }

    /**
     * Portal is number from 1 to 4
     *
     * @param portal
     */
    public void spawnBotOnPortal(int portal) {
        spawnBotWithData(new EnemyData(portals.get(portal - 1), 40));
    }

    public void killEnemy(Enemy enemy) {
        ((GameStage)(Facade.getGame().getCurrentScreen().getStage())).getUiStage().getScoreScript().setScore();
        ((GameStage)(Facade.getGame().getCurrentScreen().getStage())).getUiStage().getBonusProgressBarScript().addProgress();
        enemy.die();
        enemies.remove(enemy);
        enemySystem.free(enemy);
    }

    public void groundExplossion() {
        ParticleActor particleActor =   new ParticleActor("ground-explossion");
        particleActor.setPosition(getCenterPos().x, getCenterPos().y);
        particleActor.start();
        addActor(particleActor);
        earthQuake();
    }

    private void earthQuake() {
        clearActions();
        addAction(Actions.sequence(
                Actions.delay(0.15f),
                Actions.moveBy(5,0,0.02f),
                Actions.moveBy(-10,0,0.02f),
                Actions.moveBy(10,0,0.025f),
                Actions.moveBy(-10,0,0.025f),
                Actions.moveBy(10,0,0.03f),
                Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        ParticleActor particleActor =   new ParticleActor("earthquake-effect");
                        particleActor.setPosition(getStage().getWidth()/4, getStage().getHeight()+getStage().getHeight()/4);
                        particleActor.start();
                        addActor(particleActor);
                    }
                }),
                Actions.moveBy(-10,0,0.03f),
                Actions.moveBy(5,0,0.04f),
                Actions.delay(0.75f),
                Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        ParticleEffect particleEffect   =   new ParticleEffect(Facade.getGame().getRm().getParticleEffect("earthquake-effect"));
                        ParticleActor particleActor     =   new ParticleActor(particleEffect,1);
                        particleActor.setPosition(getStage().getWidth()/4*3, getStage().getHeight()+getStage().getHeight()/2);
                        particleActor.start();
                        addActor(particleActor);
                    }
                })

        ));


    }
}
