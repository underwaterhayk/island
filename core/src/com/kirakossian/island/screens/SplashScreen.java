package com.kirakossian.island.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.kirakossian.island.Facade;
import com.kirakossian.island.Island;
import com.kirakossian.island.stages.SplashStage;

/**
 * Created by kirakos on 12/17/17.
 */
public class SplashScreen extends AbstractScreen {

    private final SpriteBatch batch;
    private  Island game;
    private TextureRegion splashFull;
    private float imgHeight;
    private float imgWidth;
    private boolean resourceManagerLoaded;
    private float timeOut;
    private boolean prepareStarted;

    public SplashScreen(Island game) {
        super();
        this.game   =   game;
        batch = new SpriteBatch();
        loadSplashAssets();
        calculateRatio();
        stage   =   new SplashStage();
    }
    @Override
    public void render(float delta) {


        Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();
        batch.draw(splashFull, Gdx.graphics.getWidth()/2 - imgWidth/2, Gdx.graphics.getHeight()/2f - imgHeight/2, imgWidth, imgHeight);
        batch.end();


        boolean assetManagerCompletedLoading = game.getRm().getAssetManager().update(100);

        if (!assetManagerCompletedLoading) {


        } else {

            if (!resourceManagerLoaded) {
                game.getRm().init();
                resourceManagerLoaded = true;
            }
            //Loading is done, lets transition to the game screen
            if (timeOut > 0.4f){
                if (!prepareStarted){
                    game.prepareGame();
                    prepareStarted = true;
                }
                if (true){ // TODO: 12/17/17 savedatamanager game datamanager loding
                    game.setGameScreen();
                }
            }
            timeOut+=delta;

        }

    }

    private void calculateRatio() {
        float imgRatio = splashFull.getRegionWidth()*1f/splashFull.getRegionHeight()*1f;
        float screenRatio = Gdx.graphics.getWidth()*1f/Gdx.graphics.getHeight()*1f;
        if (imgRatio > screenRatio){
            // screen is taller so base is height
            imgHeight = Gdx.graphics.getHeight();
            float ratio = 1f*Gdx.graphics.getHeight()/splashFull.getRegionHeight();
            imgWidth = ratio*splashFull.getRegionWidth();
        } else {
            // image is taller so base is width
            imgWidth = Gdx.graphics.getWidth();
            float ratio = 1f*Gdx.graphics.getWidth()/splashFull.getRegionWidth();
            imgHeight = ratio*splashFull.getRegionHeight();
        }
    }
    private void loadSplashAssets() {
        splashFull = new TextureRegion(new Texture(Gdx.files.internal("splash/splashFull.jpg")));
        splashFull.getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
    }

}
