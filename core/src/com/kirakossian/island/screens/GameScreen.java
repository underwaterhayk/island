package com.kirakossian.island.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.esotericsoftware.spine.*;
import com.kirakossian.island.Facade;
import com.kirakossian.island.stages.GameStage;
import com.kirakossian.island.stages.UIStage;


/**
 * Created by kirakos on 12/17/17.
 */
public class GameScreen extends AbstractScreen {
    private UIStage uiStage;
    private InputMultiplexer inputMultiplexer;

    public GameScreen() {
        super();
        uiStage =   new UIStage();
        stage   =   new GameStage();
        ((GameStage)stage).setUIStage(uiStage);

        inputMultiplexer    =   new InputMultiplexer();
        inputMultiplexer.addProcessor(uiStage);
        inputMultiplexer.addProcessor(stage);
        Gdx.input.setInputProcessor(inputMultiplexer);

    }
    @Override
    public void render(float delta) {
        super.render(delta);
        if(stage!=null) {
            stage.act();
            stage.draw();
        }
        if(uiStage!=null) {
            uiStage.act();
            uiStage.draw();
        }
    }

    @Override
    public void dispose() {
        super.dispose();
        if(stage!=null){
            stage.dispose();
        }
        if(uiStage!=null){
            uiStage.dispose();
        }
        super.dispose();

    }

    public UIStage getUiStage() {
        return uiStage;
    }
}
