package com.kirakossian.island.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;

/**
 * Created by kirakos on 12/17/17.
 */
public class AbstractScreen implements Screen {

    protected Stage stage;

    public AbstractScreen() {

    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
//        if(stage!=null){
//            stage.act();
//            stage.draw();
//        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }


    @Override
    public void dispose() {
//        if(stage != null){
//            stage.dispose();
//        }
    }

    public Stage getStage() {
        return stage;
    }
}
