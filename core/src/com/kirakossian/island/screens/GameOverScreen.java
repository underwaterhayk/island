package com.kirakossian.island.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.kirakossian.island.stages.GameOverStage;
import com.kirakossian.island.stages.GameStage;
import com.kirakossian.island.stages.UIStage;


/**
 * Created by kirakos on 12/17/17.
 */
public class GameOverScreen extends AbstractScreen {
    private InputMultiplexer inputMultiplexer;

    public GameOverScreen() {
        super();
        stage   =   new GameOverStage();

        inputMultiplexer    =   new InputMultiplexer();
        inputMultiplexer.addProcessor(stage);
        Gdx.input.setInputProcessor(inputMultiplexer);

    }
    @Override
    public void render(float delta) {
        super.render(delta);
        if(stage!=null) {
            stage.act();
            stage.draw();
        }

    }

    @Override
    public void dispose() {
        super.dispose();
        if(stage!=null){
            stage.dispose();
        }
        super.dispose();
    }

}
